package com.abocode.smarten.rpc.server.netty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Description:
 *
 * @author: guanxianfei
 * @date: 2019/7/23
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ConfigProperties {
    private String projectCode;
    private String profile;
    private String[] moduleCodes;
}
