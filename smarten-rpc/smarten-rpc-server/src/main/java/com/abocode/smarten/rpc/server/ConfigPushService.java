package com.abocode.smarten.rpc.server;

/**
 * Description:
 * @author: guanxianfei
 * @date: 2019/7/25
 */
public interface ConfigPushService {
    void pushConfig(String projectCode, String profile, String[] moduleCodes);
}
