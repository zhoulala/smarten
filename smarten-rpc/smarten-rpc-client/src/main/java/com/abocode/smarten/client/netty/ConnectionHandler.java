package com.abocode.smarten.client.netty;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

import java.nio.charset.StandardCharsets;

/**
 * Description:
 * Define a  {@code ConnectionHandler} extends {@link ChannelInboundHandlerAdapter}.
 *
 * @author: guanxianfei
 * @date: 2019/7/24
 */
public class ConnectionHandler extends ChannelInboundHandlerAdapter {

    private String clientMsg;

    public ConnectionHandler(String clientMsg) {
        this.clientMsg = clientMsg;
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) {
        String msg = clientMsg + "\r\n";
        ByteBuf encoded = Unpooled.copiedBuffer(msg, StandardCharsets.UTF_8);
        ctx.channel().writeAndFlush(encoded);
    }
}
