package com.abocode.smarten.server.service.impl;

import com.abocode.smarten.rpc.server.ConfigPushService;
import com.abocode.smarten.rpc.server.PushService;
import com.abocode.smarten.server.service.ConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Description:
 * Define a  {@code ConfigPushServiceImpl} implementations {@link ConfigPushService}.
 *
 * @author: guanxianfei
 * @date: 2019/7/25
 */
@Service
public class ConfigPushServiceImpl  implements ConfigPushService {
    @Autowired
    private ConfigService configService;
    @Autowired
    private PushService pushService;
    @Override
    public void pushConfig(String projectCode, String profile, String[] moduleCode) {
        String  properties=configService.findAllConfigProperties();
        pushService.pushAll(properties);
    }
}
