package com.abocode.smarten.server;

import com.abocode.smarten.server.configuration.ServerConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Description:
 * @author: guanxianfei
 * @date: 2019/7/23
 */
@SpringBootApplication
@Import({ServerConfiguration.class})
@EnableJpaAuditing
@EnableTransactionManagement
public class SmartenServer {
    public static void main(String[] args) {
        SpringApplication application = new SpringApplication(SmartenServer.class);
        application.run();
    }
}
