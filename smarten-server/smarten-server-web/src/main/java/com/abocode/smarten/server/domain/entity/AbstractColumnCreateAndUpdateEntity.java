package com.abocode.smarten.server.domain.entity;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.util.Date;

/**
 * Description:
 * 定义表通用字段：创建、修改操作字段
 * @author: guanxianfei
 * @date: 2019/1/10
 */
@Getter
@Setter
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public abstract class AbstractColumnCreateAndUpdateEntity implements Serializable {
    @Column(columnDefinition="varchar(20) COMMENT '创建人'")
    private  String createBy;
    @Column(nullable = false)
    @CreatedDate
    private Date createTime;
    @Column(columnDefinition="varchar(20) COMMENT '修改人'")
    private  String updateBy;
    @Column(nullable = false)
    @LastModifiedDate
    private Date updateTime;
}
