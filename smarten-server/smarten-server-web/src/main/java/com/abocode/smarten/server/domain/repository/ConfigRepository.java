package com.abocode.smarten.server.domain.repository;

import com.abocode.smarten.server.domain.entity.Config;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Description:
 * Define a  {@code ConfigRepository}
 *
 * @author: guanxianfei
 * @date: 2019/1/5
 */
@Repository
public interface ConfigRepository extends JpaRepository<Config, Long> {
    Config findByCode(String code);

    long countByCode(String code);
}
