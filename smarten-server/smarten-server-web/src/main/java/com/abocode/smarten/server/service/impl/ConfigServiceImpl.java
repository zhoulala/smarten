package com.abocode.smarten.server.service.impl;
import com.abocode.smarten.rpc.server.ConfigPushService;
import com.abocode.smarten.server.api.command.CreateConfigCommand;
import com.abocode.smarten.server.api.command.GetConfigCommand;
import com.abocode.smarten.server.api.command.UpdateConfigCommand;
import com.abocode.smarten.server.domain.entity.Config;
import com.abocode.smarten.server.domain.repository.ConfigRepository;
import com.abocode.smarten.server.service.ConfigService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.*;

/**
 * @author: guanxianfei
 * @date: 2019/1/5
 */
@Service
@Slf4j
public class ConfigServiceImpl implements ConfigService {
	@Autowired
	private ConfigRepository configRepository;
	@Autowired
	private ConfigPushService configPushService;

	@Override
	public void add(CreateConfigCommand createConfigCommand) {
		assertNotExist(createConfigCommand.getCode());
		Config config = new Config();
		BeanUtils.copyProperties(createConfigCommand, config);
		configRepository.save(config);
		String[] moduleCodes={config.getModuleCode()};
		configPushService.pushConfig(config.getProjectCode(),config.getProfile(),moduleCodes);
	}




	private void assertNotExist(String code) {
		long num = configRepository.countByCode(code);
		Assert.isTrue(num <= 0, String.format("配置code[%s]已经存在", code));
	}


	@Override
	public String findValueByCode(String code) {
		Config config = configRepository.findByCode(code);
		Assert.notNull(config, String.format("配置code[%s]为空", code));
		return config.getValue();
	}


	@Override
	public void update(Long id, UpdateConfigCommand command) {
		Optional<Config> optional = configRepository.findById(id);
		Assert.isTrue(optional.isPresent(), String.format("配置id[%s]为空", id));
		Config config = optional.get();
		if (!config.getCode().equals(command.getCode())) {
			assertNotExist(command.getCode());
		}
		BeanUtils.copyProperties(command, config);
		configRepository.save(config);
		String[] moduleCodes={config.getModuleCode()};
		configPushService.pushConfig(config.getProjectCode(),config.getProfile(),moduleCodes);
	}


	@Override
	public GetConfigCommand find(Long id) {
		Optional<Config> optional = configRepository.findById(id);
		if (!optional.isPresent()) {
			log.info(String.format("配置id[%s]为空", id));
			return null;
		}
		Config config = optional.get();
		GetConfigCommand getConfigCommand = new GetConfigCommand();
		BeanUtils.copyProperties(config, getConfigCommand);
		return getConfigCommand;
	}

	@Override
	public String findAllConfigProperties() {
		List<Config> configList=configRepository.findAll();
		return  this.covert(configList);
	}

	private String  covert(List<Config> configList) {
		StringBuilder stringBuffer=new StringBuilder();
		for (Config config:configList){
			stringBuffer.append(config.getCode()).append("=").append(config.getValue()).append("\r\n");
		}
		return stringBuffer.toString();
	}
}
