package com.abocode.smarten.server.service;

import com.abocode.smarten.server.api.command.CreateConfigCommand;
import com.abocode.smarten.server.api.command.GetConfigCommand;
import com.abocode.smarten.server.api.command.UpdateConfigCommand;

/**

 * @author: guanxianfei
 * @date: 2019/1/5
 */
public interface ConfigService {

    void add(CreateConfigCommand createConfigCommand);

    String findValueByCode(String code);

    void update(Long id, UpdateConfigCommand command);

    GetConfigCommand find(Long id);

    String findAllConfigProperties();

}
