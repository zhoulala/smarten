package com.abocode.smarten.client;

import com.abocode.smarten.client.event.ConfigurationListener;
import org.springframework.beans.factory.FactoryBean;

import java.util.List;
import java.util.Properties;

/**
 * Description:
 * Define a  {@code ClientPropertiesFactoryBean} implementations {@link FactoryBean}.
 *
 * @author: guanxianfei
 * @date: 2019/7/24
 */
public class ClientPropertiesFactoryBean implements FactoryBean<Properties> {
    private ClientProperties clientProperties;
    public ClientPropertiesFactoryBean() {
        this(null);
    }
    public ClientPropertiesFactoryBean(List<ConfigurationListener> listeners) {
        clientProperties = new ClientProperties();
        if (listeners != null) {
            for (ConfigurationListener listener : listeners) {
                clientProperties.addConfigurationListener(listener);
            }
        }
    }


    public ClientPropertiesFactoryBean(final String projectCode, final String profile, List<ConfigurationListener> listeners) {
        clientProperties = new ClientProperties(projectCode, profile);
        if (listeners != null) {
            for (ConfigurationListener listener : listeners) {
                clientProperties.addConfigurationListener(listener);
            }
        }
    }

    public ClientPropertiesFactoryBean(String host, int port, final String projectCode, final String profile, final String moduleCodes) {
        this(host, port, projectCode, profile, moduleCodes, null);
    }

    public ClientPropertiesFactoryBean(String host, int port, final String projectCode, final String profile, final String moduleCodes, List<ConfigurationListener> listeners) {
        clientProperties = new ClientProperties(host, port, projectCode, profile, moduleCodes);
        if (listeners != null) {
            for (ConfigurationListener listener : listeners) {
                clientProperties.addConfigurationListener(listener);
            }
        }
    }

    @Override
    public Properties getObject() {
        return clientProperties.getProperties();
    }

    @Override
    public Class<?> getObjectType() {
        return Properties.class;
    }

    @Override
    public boolean isSingleton() {
        return true;
    }
}
