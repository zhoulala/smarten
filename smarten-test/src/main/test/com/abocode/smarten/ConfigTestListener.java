/**        
 * Copyright (c) 2013 by 苏州科大国创信息技术有限公司.    
 */    
package com.abocode.smarten;


import com.abocode.smarten.com.abocode.smarten.client.event.ConfigurationEvent;
import com.abocode.smarten.com.abocode.smarten.client.event.ConfigurationListener;
import lombok.AllArgsConstructor;
@AllArgsConstructor
public class ConfigTestListener implements ConfigurationListener {
	public void configurationChanged(ConfigurationEvent event) {
		System.out.println(event.getType().name() + " " + event.getPropertyName() + " " + event.getPropertyValue());
	}
}
